import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Card, Avatar, CardHeader, CardMedia, CardContent, Typography, Button  } from '@material-ui/core/';
import styles from './News.css';

class News extends Component {
  render() {
    return(
      <Card className="News">
      <CardHeader
        avatar={
          <Avatar aria-label="Recipe" src={`https://i.pravatar.cc/40?img=${this.props.id}`} />
        }
        title={this.props.title}
      />
      <CardMedia 
        className="News__image"     
        image={`https://picsum.photos/id/${this.props.id}/800/300`}
        title="Paella dish"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {this.props.body}
        </Typography>
        <br/>
        <div style={{textAlign: "right"}}>
        
          <Link to={`/posts/${this.props.id}`} style={{textDecoration: "none"}}>
            <Button variant="contained" color="primary">
            Read More
            </Button>
          </Link>
       
          
        </div>
      
      </CardContent>
    </Card>
    );
  }
}

export default News;