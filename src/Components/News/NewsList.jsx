import React, { Component } from 'react';
import { Container, Grid } from '@material-ui/core/';

import News from './News';

class NewsList extends Component {

  

  render() {
    const { news } = this.props;
    return(
      <Container>
        <h1>Latest News</h1>
        <Grid container spacing={4}>
          {news.map((item, index) => {
            if (index < 50) {
              return (
                <Grid item md={4} key={item.id}>
                  <News user={item.userId} id={item.id} title={item.title} body={item.body} />
                </Grid>
              );
            }            
          })}         
        </Grid>
      </Container>
    );
  }

}

export default NewsList;