import React, { Component } from 'react';
import { Container, Grid } from '@material-ui/core/';
import styles from './NewsDetails.css';

class NewsDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async getNewsDetails() {
    try {
      const data = await fetch(
        `https://jsonplaceholder.typicode.com/posts/${
          this.props.match.params.id
        }`
      );

      this.setState({
        news: await data.json()
      });
    } catch (error) {
      console.log(error);
    }
  }

  componentDidMount() {
    this.getNewsDetails();
  }

  render() {
    if (!this.state.news) {
      return <h1 style={{textAlign:"center"}}>loading...</h1>;
    }

    const { title, body, id } = this.state.news;
    const image = `linear-gradient(90deg, rgb(63, 81, 181) 100% ,white),url(https://picsum.photos/id/${id}/1000/400)`;
    return(
      <>
        <div className="News__banner" style={{backgroundImage:  image  }} >
          <div className="News__title">
            <Container>
              <Grid container>
                <h1>{title}</h1>
              </Grid>
            </Container>
          </div>
        </div>
        <div className="News__body">
            <Container>
              <Grid container>
                <Grid item md={4}>
                  <h4>{body}</h4>
                </Grid>
              </Grid>
            </Container>
        </div>
      </>
    );
  }
}

export default NewsDetails;