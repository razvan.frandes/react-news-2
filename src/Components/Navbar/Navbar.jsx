import React, { Component } from 'react';
import styles from './Navbar.css';
import { AppBar, Toolbar, Typography } from '@material-ui/core/';
class Navbar extends Component {
  render() {
    return (
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">
            News
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }
}

export default Navbar;