import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";

import Navbar from './Components/Navbar/Navbar';
import NewsList from './Components/News/NewsList';
import NewsDetails from './Components/News/NewsDetails';
import { number, string } from 'prop-types';

interface Post {
  id: number;
  title: string;
  body: string;
  userId: number;
}

interface State {
  news?: Post[];
}

class App extends Component<any, any> {
  
  constructor(props: any){
    super(props);
    this.state = {}
  }

  async getNews() {
    try {
      const data = await fetch("https://jsonplaceholder.typicode.com/posts/");
      const jsonData = await data.json();
      this.setState({
        news: jsonData
      });
    } catch (error) {
      console.log(error);
    }
  }

  componentDidMount() {
    this.getNews();
  }


  render() {
    if ( !this.state.news ) {
      return(
        <>
          <CssBaseline />
          <Navbar />
          <h1 style={{textAlign:"center"}}>Loading</h1>
        </>
      )
    }
    
    const { news } = this.state;
    return (
      <BrowserRouter>
        <CssBaseline />
        <Navbar />
        <Switch>
          <Route exact path="/" render={() => <NewsList news={news} />} />
          <Route exact path="/posts/:id" component={NewsDetails} />
          <Route render={() => <h1>Page not found!</h1>} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
